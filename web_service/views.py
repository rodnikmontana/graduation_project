from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.conf import settings

from .models import UploadedFile, Post
from .forms import PostForm
import numpy as np
import rasterio
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
import io
import base64
import cv2

def index_view(request):
    username = None
    if request.user.is_authenticated:
        username = request.user.username
    context = {'username' : username}
    return render(request, 'index.html', context)

def ndvi_calculator_view(request):
    if request.method == 'POST':
        #  Проверяем, что файлы были загружены
        if 'red_band' not in request.FILES or 'nir_band' not in request.FILES:
            return redirect(reverse('ndvi_calculator'))  # Возвращаемся на главную страницу

        red_band_file = request.FILES['red_band']
        nir_band_file = request.FILES['nir_band']

        # Сохраняем загруженные файлы
        red_band_instance = UploadedFile(file=red_band_file)
        nir_band_instance = UploadedFile(file=nir_band_file)
        red_band_instance.save()
        nir_band_instance.save()

        # Получаем пути к загруженным файлам
        red_band_path = red_band_instance.file.path
        nir_band_path = nir_band_instance.file.path
        
        # Рассчитываем NDVI
        with rasterio.open(red_band_path) as red_src, rasterio.open(nir_band_path) as nir_src:
            red_band = red_src.read(1)  # Красный канал
            nir_band = nir_src.read(1)  # Ближний инфракрасный канал

            # Игнорируем деление на ноль и значения NaN
            np.seterr(divide='ignore', invalid='ignore')
            ndvi = (nir_band - red_band) / (nir_band + red_band)
        
        # Визуализация NDVI
        cmap = LinearSegmentedColormap.from_list('ndvi', ['red', 'yellow', 'green'])
        plt.figure(figsize=(10, 10))
        plt.imshow(ndvi, cmap=cmap)
        plt.colorbar(label='NDVI')
        plt.title('NDVI Visualization')
        plt.axis('off')
        
        # Преобразование графика в изображение для отображения на странице
        buffer = io.BytesIO()
        plt.savefig(buffer, format='png')
        buffer.seek(0)
        image_png = buffer.getvalue()
        buffer.close()

        # Преобразование изображения в строку Base64
        ndvi_graphic = base64.b64encode(image_png).decode('utf-8')
        ndvi_graphic = 'data:image/png;base64,{}'.format(ndvi_graphic)
        
        plt.close()
        
        return render(request, 'ndvi_result.html', {'ndvi_graphic': ndvi_graphic})
    return render(request, 'ndvi_calculator.html')

def ndwi_calculator_view(request):
    if request.method == 'POST':
        # Проверяем, что файлы были загружены
        if 'green_band' not in request.FILES or 'nir_band' not in request.FILES:
            return redirect(reverse('ndwi_calculator'))  # Возвращаемся на страницу калькулятора

        green_band_file = request.FILES['green_band']
        nir_band_file = request.FILES['nir_band']

        # Сохраняем загруженные файлы
        green_band_instance = UploadedFile(file=green_band_file)
        nir_band_instance = UploadedFile(file=nir_band_file)
        green_band_instance.save()
        nir_band_instance.save()

        # Получаем пути к загруженным файлам
        green_band_path = green_band_instance.file.path
        nir_band_path = nir_band_instance.file.path

        # Рассчитываем NDWI
        with rasterio.open(green_band_path) as green_src, rasterio.open(nir_band_path) as nir_src:
            green_band = green_src.read(1)  # Зеленый канал (B03)
            nir_band = nir_src.read(1)  # Ближний инфракрасный канал (B08)

            # Игнорируем деление на ноль и значения NaN
            np.seterr(divide='ignore', invalid='ignore')
            ndwi = (green_band - nir_band) / (green_band + nir_band)

        # Визуализация NDWI
        cmap = LinearSegmentedColormap.from_list('ndwi', ['blue', 'white', 'green'])
        plt.figure(figsize=(10, 10))
        plt.imshow(ndwi, cmap=cmap)
        plt.colorbar(label='NDWI')
        plt.title('NDWI Visualization')
        plt.axis('off')

        # Преобразование графика в изображение для отображения на странице
        buffer = io.BytesIO()
        plt.savefig(buffer, format='png')
        buffer.seek(0)
        image_png = buffer.getvalue()
        buffer.close()

        # Преобразование изображения в строку Base64
        ndwi_graphic = base64.b64encode(image_png).decode('utf-8')
        ndwi_graphic = 'data:image/png;base64,{}'.format(ndwi_graphic)

        plt.close()

        return render(request, 'ndwi_result.html', {'ndwi_graphic': ndwi_graphic})

    return render(request, 'ndwi_calculator.html')

def detect_field_boundaries(request):
    if request.method == 'POST':
        # Проверяем, что файл был загружен
        if 'field_image' not in request.FILES:
            return redirect(reverse('detect_field_boundaries'))  # Возвращаемся на главную страницу

        field_image_file = request.FILES['field_image']

        # Чтение изображения из буфера
        nparr = np.frombuffer(field_image_file.read(), np.uint8)
        image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

        # Обработка изображения
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        edges = cv2.Canny(gray_image, 50, 150)

        # Преобразование изображения в строку PNG
        _, buffer = cv2.imencode('.png', edges)
        image_png = buffer.tobytes()

        # Преобразование изображения в строку Base64
        graphic = base64.b64encode(image_png).decode('utf-8')
        graphic = 'data:image/png;base64,{}'.format(graphic)

        return render(request, 'field_bounds_result.html', {'graphic': graphic})

    return render(request, 'field_boundaries.html')

def ndmi_calculator_view(request):
    if request.method == 'POST':
        if 'swir_band' not in request.FILES or 'nir_band' not in request.FILES:
            return redirect(reverse('ndmi_calculator'))
        
        swir_band_file = request.FILES['swir_band']
        nir_band_file = request.FILES['nir_band']
        
        swir_band_instance = UploadedFile(file=swir_band_file)
        nir_band_instance = UploadedFile(file=nir_band_file)
        swir_band_instance.save()
        nir_band_instance.save()
        
        swir_band_path = swir_band_instance.file.path
        nir_band_path = nir_band_instance.file.path
        
        with rasterio.open(swir_band_path) as swir_src, rasterio.open(nir_band_path) as nir_src:
            swir_band = swir_src.read(1)
            nir_band = nir_src.read(1)
            
            np.seterr(divide='ignore', invalid='ignore')
            ndmi = (swir_band - nir_band)/(swir_band + nir_band)
            
        cmap = LinearSegmentedColormap.from_list('ndmi', ['#FFFFFF', '#4CAF50', '#2196F3'])
        plt.figure(figsize=(10, 10))
        plt.imshow(ndmi, cmap=cmap)
        plt.colorbar(label='NDMI')
        plt.title('NDMI Visualization')
        plt.axis('off')
        
        buffer = io.BytesIO()
        plt.savefig(buffer, format='png')
        buffer.seek(0)
        image_png = buffer.getvalue()
        buffer.close()
        
        ndmi_graphic = base64.b64encode(image_png).decode('utf-8')
        ndmi_graphic = 'data:image/png;base64,{}'.format(ndmi_graphic)
        
        plt.close()
        
        return render(request, 'ndmi_result.html', {'ndmi_graphic' : ndmi_graphic})
    
    return render(request, 'ndmi_calculator.html')

def sign_up_view(request):
    return render(request, 'sign_up.html')


# CRUD OPERATIONS:
# Create:
def post_create_view(request):
    if request.method == 'GET':
        form = PostForm()
        return render(request, 'post_create.html', context={'form' : form})
    elif request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            new_post = Post.objects.create(
                title = form.cleaned_data['title'],
                body = form.cleaned_data['body'],
                author = request.user)
                # author = form.cleaned_data['author'])
            
            return redirect('post_view', pk=new_post.pk)
        else:
            return render(request, 'post_create.html', context={
                'from' : form})
        
# Read:
def posts_list_view(request):
    post = Post.objects.all()
    return render(request, 'posts_list.html', context={
        'posts' : post
    })

def post_view(request, *args, **kwargs):
    post = get_object_or_404(Post, pk=kwargs.get('pk'))
    return render(request, 'post_view.html',
                  context={'post': post})

# Update

from django.contrib.auth.decorators import login_required

@login_required
def post_update(request, *args, **kwargs):
    post = get_object_or_404(Post, pk=kwargs.get('pk'))
    
    if request.method == 'GET':
        form = PostForm(initial={
            'title': post.title,
            'body': post.body
        })
        return render(request, 'post_update.html', context={'form': form, 'post': post})
    
    elif request.method == 'POST':
        form = PostForm(data=request.POST)
        
        if form.is_valid():
            post.title = form.cleaned_data.get('title')
            post.body = form.cleaned_data.get('body')
            # Устанавливаем автора напрямую на текущего пользователя
            post.author = request.user
            post.save()
            return redirect('post_view', pk=post.pk)
        else:
            return render(request, 'post_update.html', context={'form': form, 'post': post})
        
# Delete

def post_delete(request, *args, **kwargs):
    post = get_object_or_404(Post, pk=kwargs.get('pk'))
    if request.method == 'GET':
        return render(request, 'post_delete.html', context={'post' : post})
    elif request.method == 'POST':
        post.delete()
        return redirect('posts_list')