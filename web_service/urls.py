from django.urls import include, path
from web_service.views import (
    ndvi_calculator_view,
    ndwi_calculator_view,
    detect_field_boundaries,
    ndmi_calculator_view,
    posts_list_view,
    post_create_view,
    post_view,
    post_update,
    post_delete,
)

urlpatterns = [
    path('ndvi_calculator/', ndvi_calculator_view, name='ndvi_calculator'),
    path('ndwi_calculator/', ndwi_calculator_view, name = 'ndwi_calculator'),
    path('detect_field_boundaries/', detect_field_boundaries, name= 'detect_field_boundaries'),
    path('ndmi_calculator/', ndmi_calculator_view, name = 'ndmi_calculator'),
    path('accounts/', include('accounts.urls')),
    path('posts_list/', posts_list_view, name='posts_list'),
    path('create_post/', post_create_view, name='create_post'),
    path('post_view/post_<int:pk>/', post_view, name='post_view'),
    path('update/<int:pk>/', post_update, name='post_update'),
    path('delete/<int:pk>/', post_delete, name='post_delete')
]